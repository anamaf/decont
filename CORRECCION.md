### download.sh

- [-1] El script debería descomprimir el archivo descargado si el tercer argumento contiene "yes". En este caso eso está hecho "a mano" en pipeline.sh.

### pipeline.sh

- [-1] Falta la generación del log final, conteniendo información de cutadapt y star

### merge.sh

- [0] Sería más seguro usar *.fastq.gz, para prevenir tomar archivos erróneos.
