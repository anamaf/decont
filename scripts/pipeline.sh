echo "######## Starting pipeline at $(date +'%H:%M:%S')... ########"
# Al ejecutar el script no olvidar redirigirlo con >> 
mkdir -p res/contaminants_idx
mkdir -p out/merged
mkdir -p log/cutadapt
mkdir -p out/trimmed

echo "Descargando genoma..."
#Download all the files specified in data/filenames
for url in $(cat data/urls) #TODO
do
    echo "Descargando url $url..."
    bash scripts/download.sh $url data  
    echo "Descargada"
done
echo "Genoma descargado"
# Download the contaminants fasta file, and uncompress it
echo "Descargando fichero con contaminantes" 
bash scripts/download.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res yes  #TODO

gunzip -k res/*.fasta.gz
echo "Descarga de los contaminantes finalizada"

echo "Indexando los contaminantes"
# Index the contaminants file
mkdir -p res/contaminants_idx
bash scripts/index.sh res/contaminants.fasta res/contaminants_idx
echo "Indice de los contaminantes completado"

echo "Uniendo las muestras en un archivo único"
mkdir -p out/merged
# Merge the samples into a single file
for sid in $(ls data/*.fastq.gz | cut -d"-" -f1 | sed "s:data/::" | sort | uniq) #TODO
do
    bash scripts/merge_fastqs.sh data out/merged $sid
done
echo "Unión de las muestras en un archivo único realizada"

mkdir -p log/cutadapt
mkdir -p out/trimmed
echo "Eliminando los adaptadores de las secuencias"
# TODO: run cutadapt for all merged files
for sample in $(ls out/merged/*.fastq.gz | cut -d"." -f1 | sed "s:out/merged/::" | sort | uniq)
do
    cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/trimmed/${sample}.trimmed.fastq.gz out/merged/${sample}.fastq.gz > log/cutadapt/${sample}.log 
done
echo "Adaptadores eliminados"

#TODO: run STAR for all trimmed files
mkdir -p out/star
echo "Realizando alineamiento con STAR"
for fname in $(ls out/trimmed/*.fastq.gz | cut -d"." -f1 | sed "s:out/trimmed/::" | sort | uniq)
do

mkdir -p out/star/${fname}
STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn out/trimmed/${fname}.trimmed.fastq.gz --readFilesCommand gunzip -c --outFileNamePrefix out/star/${fname}/
done 
echo "Alineamiento finalizado"

# TODO: create a log file containing information from cutadapt and star logs
# (this should be a single log file, and information should be *appended* to it on each run)
# - cutadapt: Reads with adapters and total basepairs
# - star: Percentages of uniquely mapped reads, reads mapped to multiple loci, and to too many loci
# tip: use grep to filter the lines you're interested in
